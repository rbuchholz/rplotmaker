# How to execute the unit tests

> ./test.sh

This triggers rplotmaker with various inputs. Check the output for errors.

# How to send patches

Please note that this project practices Semantic Versioning and [Dependable API Evolution](https://github.com/dwt/Dependable_API_Evolution)

# Release checklist

- Tests run with py2 and latest py3
- Increment version and tag
- upload new build with `rm -r dist/* && python -m build --sdist --wheel && twine upload dist/*`
- try upload to pypi with `twine upload --repository testpypi`
