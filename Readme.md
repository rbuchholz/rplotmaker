# rplotmaker: small wrapper around R to ease creating plots from r code.

rplotmaker is intended to make it easier to embedd R into python applications that just need the file to create a plot into a file.

The R file in question already needs to generate a plot, rplotmaker just adds some magic to get the output of the R computation into the desired file.

See `examples` for how it expects R files to behave.
