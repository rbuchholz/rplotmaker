#!/bin/sh

rm -rf output/*

die()
{
    echo $@
    exit 1
}

set -ex

rplotmaker examples/empty.r output/empty.png && die "should fail as no graph is created"
test -s output/empty.png && die "output/empty.png shoudl not have been created"

rplotmaker examples/sin.r output/sin.png && test -s output/sin.png || die "file output/sin.png not existent or of size zero"

rplotmaker examples/exploding.r output/exploding.png && die "should exit with error code, did exit with $?"
test -s output/exploding.png && die "output/exploding.png should not have been created"

rplotmaker --timeout 0 examples/slow.r output/slow.png && die "shoud exit with error code, did exit with $?"
test -s output/exploding.png && die "output/slow.png should not have been created"

rplotmaker --timeout 20 examples/slow.r output/slow.png || die "should succeed"
test -s output/slow.png || die "output/slow.png missing or of zero size"


rm -rf output/*
